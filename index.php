<?php
/*
|----------------------------------------------------------------
| Facebook Export Page Events
|----------------------------------------------------------------
| This is heavily inspired by the answer on
| http://webapps.stackexchange.com/questions/29136/exporting-facebook-page-events-to-google-calendar
| 
| This script does EXPORT ALL EVENTS for the given facebook page (including the past events)  
| into iCal file, which can be easily used to import the events into Google Calendar / iCal / etc.
| 
| Requirements: 
|   1. facebook SDK - https://github.com/facebook/facebook-php-sdk
|   2. iCalcreator - http://kigkonsult.se/iCalcreator/
| 
| Made by:  
|   Igor Rjabinin ( http://igo.sk )
| 
*/


// --------------------------------------------------------------
// This is the place to define configuration
// --------------------------------------------------------------

$page_name = ''; // doesn't have to be exact -> will be used f.e. as filename 
$page_id = '';  // how to get it? check "id" at http://graph.facebook.com/page_name 
$app_id = ''; // your facebook app id. if you don't have it, register at http://developers.facebook.com
$secret = ''; // your facebook app secret
$add_fb_link = true; // include link to the event at the end of the description

date_default_timezone_set('Europe/Bratislava');

// --------------------------------------------------------------
// Check the relative path to the fb-sdk and icalcreator
// --------------------------------------------------------------

require 'fb-sdk/src/facebook.php'; // https://github.com/facebook/facebook-php-sdk
require 'icalcreator/iCalcreator.class.php'; // http://kigkonsult.se/iCalcreator/

// --------------------------------------------------------------
// END OF USER CONFIGURATION. FOLLOWING CODE JUST FOR NERDS
// --------------------------------------------------------------
/*





            Why it 
             stopped working?      I told you..... 
                      `__      _  ,
                   >''(/..    OO-
                       |/   .. \|  ,,          
                      //>>   \V/ \V/         
                      ||_\     |_|           
                      "||      | |           
    __________________ kk ____ d b ______________jg




Nerd Boy by Joaquim Gandara              
*/

$facebook = new Facebook(array(
    'appId'  => $app_id,
    'secret' => $secret,
    'cookie' => true, // enable optional cookie support 
));

$fql    =   "SELECT name, pic, start_time, end_time, location, description, eid FROM event WHERE eid IN ( SELECT eid FROM event_member WHERE uid = $page_id and start_time>0) order by start_time DESC";

$param  =   array(
    'method'    => 'fql.query',
    'query'     => $fql,
    'callback'  => ''
);
$fqlResult   =  $facebook->api($param);

// ###########################################################

$c = new vcalendar(array('unique_id' => $page_name));

$c->setProperty('X-WR-CALNAME', $page_name . ' events');
$c->setProperty('X-WR-CALDESC', 'Facebook events for ' . $page_name);
// $c->setProperty('X-WR-TIMEZONE', $events[0]->timezone); // We assume all of the events use the same timezone.

// Loop through the events, create event components in the calendar
foreach($fqlResult as $key => $event){
    $e[$key] = &$c->newComponent('vevent');        
    $e[$key]->setProperty('summary', $event['name']);
    $e[$key]->setProperty('dtstart', $event['start_time']);
    if (!empty($event['end_time'])) $e[$key]->setProperty('dtend', $event['end_time']); // beware! google calendar will reject import if there is event with empty end_time - so better not to set it
    if ($add_fb_link) $event['description'] .= "\n\nhttp://www.facebook.com/events/" . $event['eid'];
    $e[$key]->setProperty('description', $event['description']);
    $e[$key]->setProperty('location', $event['location']);
}

header("Content-Type: text/calendar; charset=UTF-8");
header("Content-Disposition: filename=" . $page_name . ".ics");
echo $c->createCalendar();
?>