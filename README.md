# Facebook Export Page Events

Just simple PHP script that EXPORT ALL EVENTS for the given FACEBOOK PAGE (including the past events)  
into iCal file, which can be easily used  to import the events into Google Calendar / iCal / etc.

## Requirements: 

- facebook SDK - [https://github.com/facebook/facebook-php-sdk](https://github.com/facebook/facebook-php-sdk)
- iCalcreator - [http://kigkonsult.se/iCalcreator/](http://kigkonsult.se/iCalcreator/)
- having facebook developer account

This is heavily inspired by the answer on
[http://webapps.stackexchange.com/questions/29136/exporting-facebook-page-events-to-google-calendar](http://webapps.stackexchange.com/questions/29136/exporting-facebook-page-events-to-google-calendar)

## Support:
I would be glad for any kind of feedback. 

(If you need a help with implementing - I can provide a support.)